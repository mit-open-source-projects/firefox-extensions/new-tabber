var extension_on = true;


function message_active_tab(tabs) { // Sending the message to the content-script
    browser.tabs.sendMessage(tabs[0].id, {});
}

function turn_extension_on_or_off() {
    if (extension_on == true) { // If we are turning the extension off
        browser.browserAction.setIcon({path: "icons/off3-32.png"});
        extension_on = false;
    } else { // If we are turning the extension on
        browser.browserAction.setIcon({path: "icons/on3-32.png"});
        extension_on = true;
    }
    // Setting the query up for informing the content-script of the change
    var querying = browser.tabs.query({
        active: true,
        currentWindow: true
    });
    querying.then(message_active_tab);
}

function handleMessage(request, sender, sendResponse) {
    sendResponse({response: extension_on});
}

browser.runtime.onMessage.addListener(handleMessage);
browser.browserAction.onClicked.addListener(turn_extension_on_or_off);
