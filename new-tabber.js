var moved = false;
var extension_on = true;


function handle_status_response(message) {
    extension_on = message.response;
    update_page();
}

function update_page() {
    if (extension_on == true) {
        edit_links();
    } else {
        reset_links();
    }
}

function update_extension_status() {
    var sending = browser.runtime.sendMessage({}); // requesting the extension status from the background script
    sending.then(handle_status_response);
}

function toggle_extension() {
    if (extension_on == true) { // if we are turning the extension off
        extension_on = false;
        reset_links();
    } else {
        extension_on = true; // if we are turning the extension on
        edit_links();
    }
}

function reset_links() { // resetting the open in new tab links to keep them staying on the same page
    var links = document.getElementsByTagName("a");
    for (var i = 0, max = links.length; i < max; i++) {
        links[i].removeAttribute('target');
    }
}

// adds the _blank target value to links so that they open in a new tab (the selling point of the code)
function edit_links() {
    var links = document.getElementsByTagName("a");
    for (var i = 0, max = links.length; i < max; i++) {
        links[i].setAttribute('target', '_blank'); // this is what makes them open in a new tab
    }
}

// event listener for when a user moves their mouse (we had to use this because DOMContentLoaded was not working)
document.addEventListener("mousemove", function () {
    if (moved == false) {
        update_extension_status();
        moved = true;
    }
});

// event listener for when users toggle the extension on or off from its icon
browser.runtime.onMessage.addListener(toggle_extension);
