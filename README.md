# New Tabber Firefox Extension 

Always open links in a new tab with this extension


## Installation

- [Download Our Extension From the Firefox Store](https://addons.mozilla.org/en-US/firefox/addon/new-tabber/)

## Disclaimer/Attribution

The icons and logos used by this extension were made by [Freepik]([https://www.flaticon.com/authors/freepik) from www.flaticon.com.

